
public class Matrice {

	private int[][] matrice;
	private int _lines;
	private int _columns;
	
	public Matrice(int n, int m) {
		this.matrice = new int[n][m];
		this._lines = n;
		this._columns = m;
	}
	
	public int get(int n, int m) {
		return matrice[n][m];
	}
	
	public void set(int n, int m, int value) {
		matrice[n][m] = value;
	}
	
	public void setMin(int n, int m, int v1, int v2, int v3) {
		set(n, m, Math.min(v1, Math.min(v2, v3)));
	}
	
	public int lines() {
		return _lines;
	}
	
	public int columns() {
		return _columns;
	}
	
	public void print() {
        System.out.println("Affichage :");
        for(int i = 0; i < _lines; i++){
            for(int j = 0; j < _columns; j++){
                System.out.print(matrice[i][j] + " ");
            }
            System.out.println();
        }
	}
}
