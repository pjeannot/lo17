import java.io.*;

class Saisie {
	
	 static String chaine;
	 static String filename;
	
     public static void main(String[] args) {
    	 readArgs(args);
		 Correcteur corrector = new Correcteur(chaine, filename);
		 System.out.println(corrector.getLemmes());
     }
     
     private static void readArgs(String[] args) {
    	 BufferedReader br=null;
    	 try {
    		 try {
    			 br = new BufferedReader(new InputStreamReader(System.in));
    			 System.out.print("Entrer une phrase : ");
    			 chaine=br.readLine();

    			 System.out.print("Entrer le fichier de lemmes : ");
    			 filename=br.readLine();
    		 } 
    		 catch(EOFException e) {
    			 br.close();
    		 }
    	 } 
    	 catch(IOException e) {
    		 System.out.println("IO Exception");
    	 }
     }
}