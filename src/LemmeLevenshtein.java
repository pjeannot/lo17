
public class LemmeLevenshtein extends Lemme {

	int score;
	
	public LemmeLevenshtein(String v, int score) {
		super(v);
		this.score = score;
	}
	
    public String toString() {
        return value + " (Levenshtein " + score +")";
    }
}