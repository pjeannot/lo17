
public class LemmePrefix extends Lemme {

	float score;
	
	public LemmePrefix(String v, float score) {
		super(v);
		this.score = score;
	}
	
     public String toString() {
        return value + " (Préfixe " + score + ")";
    }
}