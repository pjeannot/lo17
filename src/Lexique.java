import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

public class Lexique {

	private Hashtable<String, String> dictionnary = new Hashtable<String, String>();
	private final float prefixMinScore = 61;
	private final int prefixMinThreshold = 3;
	private final int prefixMaxThreshold = 5;
	private final int levenshteinMaxDistance = 3;
	private final int levenshteinMinThreshold = 3;
	private final int levenshteinMaxThreshold = 5;
	
	// Constructeur
	// Va lire le fichier passé en paramètre pour charger le dictionnaire de lemmes
	public Lexique(String filename) {
		
		BufferedReader br=null;
		String chaine;
		try {
			try {
				br = new BufferedReader(new FileReader(filename));
				while ((chaine=br.readLine())!=null) {
					String[] separated = chaine.split(" \t");
					dictionnary.put(separated[0], separated[1]);
				}
			} 
			catch(EOFException e) {
				br.close();
			}
		} 
		catch(FileNotFoundException e) {
			System.out.println("fichier inconnu : " + filename);
		} 
		catch(IOException e) {
			System.out.println("IO Exception");
		}
	}
	
	public HashSet<Lemme> getBestsCandidates(String word) {

		HashSet<Lemme> result = new HashSet<Lemme>();
		
		// On cherche si le mot existe déjà dans le dictionnaire
		String existingLemme = getLemmeFromDictionnary(word);
		if (existingLemme != null) {
			result.add(new Lemme(existingLemme));
			return result;
		}
		
		// On cherche si des lemmes existent en appliquant l'algo de recherche pas préfixe
		ArrayList<Lemme> prefixLemmes = getLemmeFromPrefixSearch(word);
		if (prefixLemmes != null) {
			result.addAll(prefixLemmes);
			// Ajouter les préfixes trouvés dans le dictionnaire
			return result;
		}

		// On cherche si des lemmes existent en appliquant l'algo de Levenshtein
		ArrayList<Lemme> levenlemmes = getLemmeFromLevenshtein(word);
		if (levenlemmes != null) {
			result.addAll(levenlemmes);
			// Ajouter les préfixes trouver dans le dictionnaire
			return result;
		}
		
		return result;
	}
	
	private String getLemmeFromDictionnary(String word) {
		return dictionnary.get(word);
	}
	
	
	// Algorithme par préfixe, poly p37
	private ArrayList<Lemme> getLemmeFromPrefixSearch(String word) {

		Set<String> keys = this.dictionnary.keySet();
		Map<String, Float> finalWords = new HashMap<String, Float>();
		// Pour chaque mot du dictionnaire, on évalue la proximité
		for(String key: keys) {
			String word2 = dictionnary.get(key); 
			float score = prox(word, word2);
			// Si le score est suffisant, on garde le mot et son score
			if (score > prefixMinScore) {
				finalWords.put(word2, score);
			}
		}
		
		// On garde le lemme ayant le meilleur score
		// Si aucun résultat
		if (finalWords.size() == 0) return null;
		else {
			ArrayList<String> lemmeKeys = new ArrayList<String>(finalWords.keySet());
			ArrayList<Lemme> bestLemme = new ArrayList<Lemme>();
			// Si un seul résultat, renvoyer le premier
			if (lemmeKeys.size() == 1) bestLemme.add(new LemmePrefix(lemmeKeys.get(0), finalWords.get(lemmeKeys.get(0)))); 
			// Sinon, trouver le lemme ayant le meilleur score
			else {
				float bestScore = 0;
				for (String lemmeTmp : lemmeKeys) {
					if (finalWords.get(lemmeTmp) > bestScore) {
						bestScore = finalWords.get(lemmeTmp);
						bestLemme.removeAll(bestLemme);
						bestLemme.add(new LemmePrefix(lemmeTmp, bestScore));
					}
					else if (finalWords.get(lemmeTmp) == bestScore) {
						bestLemme.add(new LemmePrefix(lemmeTmp, bestScore));
					}
				}
			}
			return bestLemme;
		}
	}
	
	private float prox(String word1, String word2) {
		int m1 = word1.length();
		int m2 = word2.length();
		if (m1 < prefixMinThreshold || m2 < prefixMinThreshold) {
			return 0;
		}
		else if (Math.abs(m1 - m2) > prefixMaxThreshold) {
			return 0;
		}
		else {
			int minLength = Math.min(m1, m2);
			int i = 0;
			while (i < minLength && word1.charAt(i) == word2.charAt(i))	i++;
			return ((float)i / (float)Math.max(m1, m2)) * 100;
		}
	}
	
	// Algo de Levenshtein, poly p39
	private ArrayList<Lemme> getLemmeFromLevenshtein(String word) {
		
		Set<String> keys = this.dictionnary.keySet();
		ArrayList<Lemme> closerLemmes = new ArrayList<Lemme>();
		int lowestDistance = 9999;
		// Pour chaque mot du dictionnaire, on évalue son score
		for(String key: keys) {
			int distance = levenshteinDistance(word, key);
			if (lowestDistance > distance && distance <= levenshteinMaxDistance) {
				closerLemmes.removeAll(closerLemmes);
				closerLemmes.add(new LemmeLevenshtein(dictionnary.get(key), distance));
				lowestDistance = distance;
			}
			else if (lowestDistance == distance && distance <= levenshteinMaxDistance) {
				closerLemmes.add(new LemmeLevenshtein(dictionnary.get(key), distance));
			}
		}
			
		return closerLemmes;
	}
	
	public int levenshteinDistance(String mA, String mB) {
		
		// Filtrer les mots s'ils sont trop courts/longs
		int m1 = mA.length();
		int m2 = mB.length();
		if (m1 < levenshteinMinThreshold || m2 < levenshteinMinThreshold) {
			return 999;
		}
		else if (Math.abs(m1 - m2) > levenshteinMaxThreshold) {
			return 999;
		}

		// Initialisation de la première ligne et première colonne
		Matrice m = new Matrice(mA.length() + 1, mB.length() + 1);
		for (int i = 0; i < m.lines(); i++) { m.set(i, 0, i); }
		for (int i = 0; i < m.columns(); i++) { m.set(0, i, i); }

		// On parcourt toute la matrice
		// Pour chaque lignes
		for (int i = 1; i < m.lines(); i++) {
			// Pour chaque colonne
			for (int j = 1; j < m.columns(); j++) {
				// Si jamais on effectue une copie ou substitution
				int delta = (mA.charAt(i-1) == mB.charAt(j-1)) ? 0 : 1;
				
				// Si inversion, on essaie d'annuler la seconde car sinon une inversion compte double
				// On compare si deux caractères 
				if (i > 1 && j > 1 && delta == 1 && mA.charAt(i-2) == mB.charAt(j-1) && mA.charAt(i-1) == mB.charAt(j-2)) {
					delta = 0;
				}
				// Calcul des valeurs
				int d1 = m.get(i-1, j-1) + delta;
				int d2 = m.get(i-1, j) + 1;
				int d3 = m.get(i, j-1) + 1;
				// Sauvegarde du minimum
				m.setMin(i, j, d1, d2, d3);
			}
		}
		m.print();
		return m.get(m.lines()-1, m.columns()-1);
	}
}
