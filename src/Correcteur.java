import java.util.ArrayList;
import java.util.HashSet;
import java.util.StringTokenizer;

public class Correcteur {

	String input;
	String filename;
	ArrayList<String> words;
	Lexique lexique;
	
	public Correcteur(String input, String filename) {
		this.input = input;
		this.filename = filename;
		tokenize();
		this.lexique = new Lexique(filename);
		System.out.println(lexique.levenshteinDistance("veux", "vuex"));
	}
	
	// Découper la phrase en input en plusieurs mots
	private void tokenize() {
        StringTokenizer st = new StringTokenizer(this.input);
        this.words = new ArrayList<String>();
        while (st.hasMoreElements()) {
          	this.words.add(st.nextElement().toString().toLowerCase());
        }
	}
	
	public HashSet<Lemme> getLemmes() {
		HashSet<Lemme> lemmes = new HashSet<Lemme>();
		for (String word : this.words) {
			lemmes.addAll(this.lexique.getBestsCandidates(word));
		}
		return lemmes;
	}
}
